#!/usr/bin/perl

use warnings;
use strict;

use feature 'say';

use Time::HiRes qw( gettimeofday tv_interval);

my $t0 = [gettimeofday];

my $maxID = 0;
my @plane = ();
while(<>){
	chomp;
	(my $row = substr $_, 0, 7) =~ tr/FB/01/;
	(my $seat = substr $_, -3) =~ tr/LR/01/;
	my $seatID = 8 * oct("0b$row") + oct("0b$seat");
	# $maxID = $seatID if $seatID > $maxID;
	push @plane, $seatID;
}
# say $maxID;

@plane = sort {$a<=>$b} @plane;
say $plane[-1];

for my $i (0..$#plane-1){
	say $plane[$i]+1 if $plane[$i+1] - $plane[$i] == 2;
}

my $duration = tv_interval($t0);
say $duration;
